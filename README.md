# tictactoe

Design of RESTful API to play tictactoe game.

# Requirements

1.Create a game.
2.Make a move by player-X or player-Y
3.Scan history of Games

# Description
This service uses a spring boot microservice and dynamodb as backend database to persist the game information.

# Build
```
mvn -clean install
```

# Package
```
docker build -f ./devops/Dockerfile . --tag=tictactoe-server:latest
```

# Deploy
```
helm install labtwin-game ./labtwin-game/
```
```
helm upgrade --install labtwin-game ./labtwin-game/
```
```
helm delete labtwin-game
```
