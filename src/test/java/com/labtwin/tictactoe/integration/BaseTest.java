package com.labtwin.tictactoe.integration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.model.*;
import com.labtwin.tictactoe.dao.GameDao;
import com.labtwin.tictactoe.service.GameService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
@AutoConfigureMockMvc
public class BaseTest extends LocalDB {

    @MockBean
    private AWSCredentials credentials;

    @MockBean
    protected DynamoDBMapper mapper;

    protected static GameService gameService;

    protected static GameDao gameDao;

    @BeforeAll
    public static void beforeAll() throws Exception {
        if (gameDao == null) {
            before();
            DynamoDBMapper dynamoDBMapper = dynamoDBMapper();
            gameDao = new GameDao(dynamoDBMapper);
            gameService = new GameService(gameDao);
        }
    }

    @AfterAll
    public static void afterAll() throws Exception {
        after();
    }


    private static DynamoDBMapper dynamoDBMapper() {
        AmazonDynamoDB client = DynamoDBEmbedded.create().amazonDynamoDB();//dynamoDB.getAmazonDynamoDB();
        client.createTable(new CreateTableRequest()
                .withTableName("tictactoe-game")
                .withKeySchema(new KeySchemaElement().withAttributeName("gameId").withKeyType(KeyType.HASH))
                .withAttributeDefinitions(
                        new AttributeDefinition().withAttributeName("gameId").withAttributeType(ScalarAttributeType.S))
                .withProvisionedThroughput(new ProvisionedThroughput(10L, 15L)));
        return new DynamoDBMapper(client, DynamoDBMapperConfig.DEFAULT);
    }
}
