package com.labtwin.tictactoe.integration.service;

import com.labtwin.tictactoe.dto.Game;
import com.labtwin.tictactoe.dto.Move;
import com.labtwin.tictactoe.dto.PlayerType;
import com.labtwin.tictactoe.exception.GameServiceException;
import com.labtwin.tictactoe.exception.NotFoundException;
import com.labtwin.tictactoe.integration.BaseTest;
import com.labtwin.tictactoe.model.PlayerInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GameServiceTest extends BaseTest {
    /**
     * Scenario: Create a game with player names
     * Expected Behaviour: Player should be created and returns a gameID
     */
    @Test
    void testCreateGame() {
        Assertions.assertNotNull(createGame());
    }

    /**
     * Scenario: Move should be made on existing game.
     * Expected Behaviour: Should throw an error if game id is not found
     */
    @Test
    void testGameIdShouldBeValid() {
        Move move = getMove(0, 0);
        Assertions.assertThrows(NotFoundException.class, () -> gameService.makeAMove("wrongId", move));
    }

    /**
     * Scenario: The moves should be alternated, i.e. X or Y cannot play consecutive time.
     * Expected Behaviour: Should pick correct player in consecutive move.
     */
    @Test
    void testMakeAMoveAndValidateAlternatePlayer() {
        String gameId = createGame();
        Game game = gameService.makeAMove(gameId, getMove(0, 0));
        Assertions.assertEquals(PlayerType.PLAYER_X, game.getLastMoveBy());

        game = gameService.makeAMove(gameId, getMove(0, 1));
        Assertions.assertEquals(PlayerType.PLAYER_Y, game.getLastMoveBy());
    }

    /**
     * Scenario: The player tries to create a move in already exising index
     * Expected Behaviour: Should get an exception if the player moves on existing index.
     */
    @Test
    void testSameIndexMoved() {
        String gameId = createGame();
        Move move = getMove(0, 0);
        Game game = gameService.makeAMove(gameId, move);
        Assertions.assertEquals(PlayerType.PLAYER_X, game.getLastMoveBy());
        Assertions.assertThrows(GameServiceException.class, () -> gameService.makeAMove(gameId, move));
    }

    /**
     * Scenario: The player X matches element horizontally
     * Expected Behaviour: The player X should be declared winner.
     */
    @Test
    void testWinnerHorizontal() {
        String gameId = createGame();
        gameService.makeAMove(gameId, getMove(0, 0));
        gameService.makeAMove(gameId, getMove(1, 1));
        gameService.makeAMove(gameId, getMove(0, 1));
        gameService.makeAMove(gameId, getMove(1, 0));
        Game game = gameService.makeAMove(gameId, getMove(0, 2));
        Assertions.assertEquals(PlayerType.PLAYER_X.name(), game.getWinnerStatus());
    }

    /**
     * Scenario: The player Y matches element horizontally
     * Expected Behaviour: The player Y should be declared winner.
     */
    @Test
    void testWinnerVerticle() {
        String gameId = createGame();
        gameService.makeAMove(gameId, getMove(1, 0));
        gameService.makeAMove(gameId, getMove(0, 1));
        gameService.makeAMove(gameId, getMove(1, 2));
        gameService.makeAMove(gameId, getMove(1, 1));
        gameService.makeAMove(gameId, getMove(2, 2));

        Game game = gameService.makeAMove(gameId, getMove(2, 1));
        Assertions.assertEquals(PlayerType.PLAYER_Y.name(), game.getWinnerStatus());
    }


    private static String createGame() {
        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.setPlayerX("player a");
        playerInfo.setPlayerY("player b");
        return gameService.createGame(playerInfo).get("gameId");
    }

    private static Move getMove(int x, int y) {
        Move move = new Move();
        move.setRow(x);
        move.setColumn(y);
        return move;
    }
}
