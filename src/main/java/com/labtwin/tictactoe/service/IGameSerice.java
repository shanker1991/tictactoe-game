package com.labtwin.tictactoe.service;

import com.labtwin.tictactoe.dto.Game;
import com.labtwin.tictactoe.dto.Move;
import com.labtwin.tictactoe.model.PlayerInfo;

import java.util.List;
import java.util.Map;

public interface IGameSerice {
    Map<String, String> createGame(PlayerInfo playerInfo);

    Game makeAMove(String gameId, Move move);

    List<Game> getAllGames(int limit);

}

